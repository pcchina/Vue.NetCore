/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹a_CourseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.AppManager.IServices;
namespace VOL.AppManager.Controllers
{
    [Route("api/a_Course")]
    [PermissionTable(Name = "a_Course")]
    public partial class a_CourseController : ApiBaseController<Ia_CourseService>
    {
        public a_CourseController(Ia_CourseService service)
        : base(service)
        {
        }
    }
}

