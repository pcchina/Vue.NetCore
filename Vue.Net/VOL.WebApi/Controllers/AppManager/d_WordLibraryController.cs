/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹d_WordLibraryController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.AppManager.IServices;
namespace VOL.AppManager.Controllers
{
    [Route("api/d_WordLibrary")]
    [PermissionTable(Name = "d_WordLibrary")]
    public partial class d_WordLibraryController : ApiBaseController<Id_WordLibraryService>
    {
        public d_WordLibraryController(Id_WordLibraryService service)
        : base(service)
        {
        }
    }
}

