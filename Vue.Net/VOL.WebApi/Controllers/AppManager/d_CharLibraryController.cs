/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹d_CharLibraryController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.AppManager.IServices;
namespace VOL.AppManager.Controllers
{
    [Route("api/d_CharLibrary")]
    [PermissionTable(Name = "d_CharLibrary")]
    public partial class d_CharLibraryController : ApiBaseController<Id_CharLibraryService>
    {
        public d_CharLibraryController(Id_CharLibraryService service)
        : base(service)
        {
        }
    }
}

