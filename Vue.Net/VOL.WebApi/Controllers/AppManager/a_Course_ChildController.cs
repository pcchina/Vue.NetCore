/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹a_Course_ChildController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.AppManager.IServices;
namespace VOL.AppManager.Controllers
{
    [Route("api/a_Course_Child")]
    [PermissionTable(Name = "a_Course_Child")]
    public partial class a_Course_ChildController : ApiBaseController<Ia_Course_ChildService>
    {
        public a_Course_ChildController(Ia_Course_ChildService service)
        : base(service)
        {
        }
    }
}

