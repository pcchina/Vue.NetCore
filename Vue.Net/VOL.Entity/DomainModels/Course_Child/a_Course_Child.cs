/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "子课程列表",TableName = "a_Course_Child")]
    public class a_Course_Child:BaseEntity
    {
        /// <summary>
       ///子课程编码
       /// </summary>
       [Key]
       [Display(Name ="子课程编码")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int Id { get; set; }

        /// <summary>
        ///父课程编码
        /// </summary>
        [Display(Name = "父课程编码")]
        [Column(TypeName = "int")]
        public int? ParentId { get; set; }

        /// <summary>
        ///子课程名称
        /// </summary>
        [Display(Name ="子课程名称")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       [Editable(true)]
       public string ChildName { get; set; }


       /// <summary>
       ///子课程老师
       /// </summary>
       [Display(Name ="子课程老师")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       [Editable(true)]
       public string ChildTeacher { get; set; }

       /// <summary>
       ///子课程排序
       /// </summary>
       [Display(Name ="子课程排序")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? ChildSort { get; set; }

       /// <summary>
       ///子课程类型
       /// </summary>
       [Display(Name ="子课程类型")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? ChildType { get; set; }

       /// <summary>
       ///子课程描述
       /// </summary>
       [Display(Name ="子课程描述")]
       [MaxLength(1024)]
       [Column(TypeName="nvarchar(1024)")]
       [Editable(true)]
       public string ChildDes { get; set; }

       /// <summary>
       ///视频标识
       /// </summary>
       [Display(Name ="视频标识")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       [Editable(true)]
       public string videoId { get; set; }

       /// <summary>
       ///是否删除
       /// </summary>
       [Display(Name ="是否删除")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? IsDelete { get; set; }

       /// <summary>
       ///是否是听
       /// </summary>
       [Display(Name ="是否是听")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? IsTrialable { get; set; }

       /// <summary>
       ///频道id
       /// </summary>
       [Display(Name ="频道id")]
       [MaxLength(32)]
       [Column(TypeName="nvarchar(32)")]
       public string ChannelId { get; set; }

       /// <summary>
       ///多用户编码
       /// </summary>
       [Display(Name ="多用户编码")]
       [Column(TypeName="int")]
       public int? TenantId { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建者编码
       /// </summary>
       [Display(Name ="创建者编码")]
       [Column(TypeName="int")]
       public int? CreateId { get; set; }

       /// <summary>
       ///视频播放地址
       /// </summary>
       [Display(Name ="视频播放地址")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       public string VideoCode2 { get; set; }

       /// <summary>
       ///视频播放地址
       /// </summary>
       [Display(Name ="视频播放地址")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       public string VideoCode1 { get; set; }

       /// <summary>
       ///子课程关联文件
       /// </summary>
       [Display(Name ="子课程关联文件")]
       [MaxLength(2048)]
       [Column(TypeName="nvarchar(2048)")]
       public string ChildRelevanceFile { get; set; }

       /// <summary>
       ///子课程长度 
       /// </summary>
       [Display(Name ="子课程长度 ")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       [Editable(true)]
       public string ChildTimeLength { get; set; }

       /// <summary>
       ///子课程开始时间
       /// </summary>
       [Display(Name ="子课程开始时间")]
       [Column(TypeName="datetime")]
       public DateTime? ChildStartTime { get; set; }

       /// <summary>
       ///子课程Url
       /// </summary>
       [Display(Name ="子课程Url")]
       [MaxLength(4096)]
       [Column(TypeName="nvarchar(4096)")]
       public string ChildImgContent { get; set; }

       /// <summary>
       ///子课程Url
       /// </summary>
       [Display(Name ="子课程Url")]
       [MaxLength(1024)]
       [Column(TypeName="nvarchar(1024)")]
       public string ChildUrl { get; set; }

       /// <summary>
       ///子课程结束时间
       /// </summary>
       [Display(Name ="子课程结束时间")]
       [Column(TypeName="datetime")]
       public DateTime? ChildEndTime { get; set; }

       /// <summary>
       ///修改时间
       /// </summary>
       [Display(Name ="修改时间")]
       [Column(TypeName="datetime")]
       public DateTime? ModifyDate { get; set; }


       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Modifier")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       public string Modifier { get; set; }


        [Display(Name = "ModifyID")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? ModifyID { get; set; }


    }
}