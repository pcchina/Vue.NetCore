/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "字列表",TableName = "d_CharLibrary",DetailTable =  new Type[] { typeof(d_WordLibrary)},DetailTableCnName = "字词列表")]
    public class d_CharLibrary:BaseEntity
    {
        /// <summary>
       ///字编号
       /// </summary>
       [Key]
       [Display(Name ="字编号")]
       [Column(TypeName= "bigint")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public long CharLibraryId { get; set; }

       /// <summary>
       ///生字
       /// </summary>
       [Display(Name ="生字")]
       [MaxLength(16)]
       [Column(TypeName="nvarchar(16)")]
       [Editable(true)]
       public string Char { get; set; }

       /// <summary>
       ///拼音
       /// </summary>
       [Display(Name ="拼音")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       [Editable(true)]
       public string Spell { get; set; }

       /// <summary>
       ///笔画
       /// </summary>
       [Display(Name ="笔画")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       public string Stroke { get; set; }

       /// <summary>
       ///字母标识
       /// </summary>
       [Display(Name ="字母标识")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       public string EnStrokes { get; set; }

       /// <summary>
       ///音频
       /// </summary>
       [Display(Name ="音频")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       public string Sound { get; set; }

       /// <summary>
       ///备注
       /// </summary>
       [Display(Name ="备注")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       public string Remark { get; set; }

       /// <summary>
       ///生词读写
       /// </summary>
       [Display(Name ="生词读写")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? Category { get; set; }

       /// <summary>
       ///笔画 
       /// </summary>
       [Display(Name ="笔画 ")]
       [MaxLength(256)]
       [Column(TypeName="nvarchar(256)")]
       public string Strokes { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateID")]
       [Column(TypeName="int")]
       public int? CreateID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Creator")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       public string Creator { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateDate")]
       [Column(TypeName="datetime")]
       public DateTime? CreateDate { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ModifyID")]
       [Column(TypeName="int")]
       public int? ModifyID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Modifier")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       public string Modifier { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ModifyDate")]
       [Column(TypeName="datetime")]
       public DateTime? ModifyDate { get; set; }

       [Display(Name ="字词列表")]
       public List<d_WordLibrary> d_WordLibrary { get; set; }

    }
}