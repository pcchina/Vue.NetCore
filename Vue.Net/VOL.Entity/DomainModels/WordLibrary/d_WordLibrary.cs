/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "字词列表", TableName = "d_WordLibrary")]
    public class d_WordLibrary : BaseEntity
    {
        /// <summary>
        ///词编号
        /// </summary>
        [Key]
        [Display(Name = "词编号")]
        [Column(TypeName = "bigint")]
        [Editable(true)]
        [Required(AllowEmptyStrings = false)]
        public long Id { get; set; }

        /// <summary>
        ///词
        /// </summary>
        [Display(Name = "词")]
        [MaxLength(64)]
        [Column(TypeName = "nvarchar(64)")]
        [Editable(true)]
        public string Word { get; set; }

        /// <summary>
        ///生字编号
        /// </summary>
        [Display(Name = "生字编号")]
        [Column(TypeName = "bigint")]
        [Editable(true)]
        public long? CharLibraryId { get; set; }

        /// <summary>
        ///生字索引
        /// </summary>
        [Display(Name = "生字索引")]
        [Column(TypeName = "int")]
        public int? Index { get; set; }

        /// <summary>
        ///词拼音
        /// </summary>
        [Display(Name = "词拼音")]
        [MaxLength(128)]
        [Column(TypeName = "nvarchar(128)")]
        [Editable(true)]
        public string WordSpell { get; set; }

        /// <summary>
        ///词笔画
        /// </summary>
        [Display(Name = "词笔画")]
        [MaxLength(128)]
        [Column(TypeName = "nvarchar(128)")]
        public string WordStroke { get; set; }

        /// <summary>
        ///词的音频
        /// </summary>
        [Display(Name = "词的音频")]
        [MaxLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string Sound { get; set; }

        /// <summary>
        ///是否重点词
        /// </summary>
        [Display(Name = "是否重点词")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? Important { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Display(Name = "CreateID")]
        [Column(TypeName = "int")]
        public int? CreateID { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Display(Name = "Creator")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string Creator { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Display(Name = "CreateDate")]
        [Column(TypeName = "datetime")]
        public DateTime? CreateDate { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Display(Name = "ModifyID")]
        [Column(TypeName = "int")]
        public int? ModifyID { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Display(Name = "Modifier")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string Modifier { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Display(Name = "ModifyDate")]
        [Column(TypeName = "datetime")]
        public DateTime? ModifyDate { get; set; }


    }
}