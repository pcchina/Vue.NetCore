/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "课程信息", TableName = "a_Course", DetailTable = new Type[] { typeof(a_Course_Child) }, DetailTableCnName = "子课程明细")]
    public class a_Course : BaseEntity
    {
        /// <summary>
        ///课程主信息编码
        /// </summary>
        [Key]
        [Display(Name = "课程主信息编码")]
        [Column(TypeName = "int")]
        [Required(AllowEmptyStrings = false)]
        public int ParentId { get; set; }

        /// <summary>
        ///主课程名称
        /// </summary>
        [Display(Name = "主课程名称")]
        [MaxLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        [Editable(true)]
        public string CourseName { get; set; }

        /// <summary>
        ///授课老师
        /// </summary>
        [Display(Name = "授课老师")]
        [MaxLength(32)]
        [Column(TypeName = "nvarchar(32)")]
        public string TeachersUserName { get; set; }

        /// <summary>
        ///课程排序
        /// </summary>
        [Display(Name = "课程排序")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? Sort { get; set; }

        /// <summary>
        ///课程价格
        /// </summary>
        [Display(Name = "课程价格")]
        [DisplayFormat(DataFormatString = "10,2")]
        [Column(TypeName = "decimal")]
        [Editable(true)]
        public decimal? Price { get; set; }

        /// <summary>
        ///是否热门 0否1 是
        /// </summary>
        [Display(Name = "是否热门 0否1 是")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? IsHot { get; set; }

        /// <summary>
        ///免费试听子课程章节ID（10,11）
        /// </summary>
        [Display(Name = "免费试听子课程章节ID（10,11）")]
        [MaxLength(64)]
        [Column(TypeName = "nvarchar(64)")]
        [Editable(true)]
        public string ChildIds { get; set; }

        /// <summary>
        ///班级分类ids
        /// </summary>
        [Display(Name = "班级分类ids")]
        [Column(TypeName = "int")]
        public int? ClassId { get; set; }

        /// <summary>
        ///关联年级id集合
        /// </summary>
        [Display(Name = "关联年级id集合")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        [Editable(true)]
        public string GradeIds { get; set; }

        /// <summary>
        ///分类标签Id集合
        /// </summary>
        [Display(Name = "分类标签Id集合")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        [Editable(true)]
        public string Cateorys { get; set; }

        /// <summary>
        ///是否优惠0否 1是
        /// </summary>
        [Display(Name = "是否优惠0否 1是")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? IsDiscount { get; set; }

        /// <summary>
        ///课程优惠价格
        /// </summary>
        [Display(Name = "课程优惠价格")]
        [DisplayFormat(DataFormatString = "10,2")]
        [Column(TypeName = "decimal")]
        [Editable(true)]
        public decimal? DiscountPrice { get; set; }

        /// <summary>
        ///苹果支付价格设置
        /// </summary>
        [Display(Name = "苹果支付价格设置")]
        [MaxLength(64)]
        [Column(TypeName = "nvarchar(64)")]
        public string Iosproductid { get; set; }

        /// <summary>
        ///是否推荐 0否 1 是
        /// </summary>
        [Display(Name = "是否推荐 0否 1 是")]
        [Column(TypeName = "int")]
        public int? IsTop { get; set; }

        /// <summary>
        ///销量
        /// </summary>
        [Display(Name = "销量")]
        [Column(TypeName = "int")]
        public int? Sales { get; set; }

        /// <summary>
        ///苹果支付价格设置
        /// </summary>
        [Display(Name = "苹果支付价格设置")]
        [DisplayFormat(DataFormatString = "10,2")]
        [Column(TypeName = "decimal")]
        public decimal? IosPayPirce { get; set; }

        /// <summary>
        ///课程状态0正常1下架 2 等等
        /// </summary>
        [Display(Name = "课程状态0正常1下架 2 等等")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? Status { get; set; }

        /// <summary>
        ///有效开始时间
        /// </summary>
        [Display(Name = "有效开始时间")]
        [Column(TypeName = "datetime")]
        public DateTime? ValidBeginTime { get; set; }

        /// <summary>
        ///有效结束时间
        /// </summary>
        [Display(Name = "有效结束时间")]
        [Column(TypeName = "datetime")]
        public DateTime? ValidEndTime { get; set; }

        /// <summary>
        ///优惠开始日期
        /// </summary>
        [Display(Name = "优惠开始日期")]
        [Column(TypeName = "datetime")]
        public DateTime? PreferentialBeginTime { get; set; }

        /// <summary>
        ///优惠结束日期
        /// </summary>
        [Display(Name = "优惠结束日期")]
        [Column(TypeName = "datetime")]
        public DateTime? PreferentialEndTime { get; set; }

        /// <summary>
        ///授课老师Id
        /// </summary>
        [Display(Name = "授课老师Id")]
        [Column(TypeName = "int")]
        public int? TeachersUserId { get; set; }

        /// <summary>
        ///内部员工价格
        /// </summary>
        [Display(Name = "内部员工价格")]
        [DisplayFormat(DataFormatString = "10,2")]
        [Column(TypeName = "decimal")]
        public decimal? InternalPrice { get; set; }

        /// <summary>
        ///苹果价格
        /// </summary>
        [Display(Name = "苹果价格")]
        [MaxLength(64)]
        [Column(TypeName = "nvarchar(64)")]
        public string IosProductidName { get; set; }

        /// <summary>
        ///图片
        /// </summary>
        [Display(Name = "图片")]
        [MaxLength(4096)]
        [Column(TypeName = "nvarchar(4096)")]
        public string CoursePic { get; set; }

        /// <summary>
        ///虚拟销量
        /// </summary>
        [Display(Name = "虚拟销量")]
        [Column(TypeName = "int")]
        public int? VirtualSales { get; set; }

        /// <summary>
        ///图片横图
        /// </summary>
        [Display(Name = "图片横图")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string CoursePicPhoneTran { get; set; }

        /// <summary>
        ///是否快递 
        /// </summary>
        [Display(Name = "是否快递 ")]
        [Column(TypeName = "int")]
        public int? IsExpress { get; set; }

        /// <summary>
        ///快递费
        /// </summary>
        [Display(Name = "快递费")]
        [DisplayFormat(DataFormatString = "10,2")]
        [Column(TypeName = "decimal")]
        public decimal? ExpressPirce { get; set; }

        /// <summary>
        ///课程类型
        /// </summary>
        [Display(Name = "课程类型")]
        [Column(TypeName = "int")]
        public int? IsComic { get; set; }

        /// <summary>
        ///课程类型
        /// </summary>
        [Display(Name = "课程类型")]
        [Column(TypeName = "int")]
        public int? IsRecord { get; set; }

        /// <summary>
        ///是否删除
        /// </summary>
        [Display(Name = "是否删除")]
        [Column(TypeName = "int")]
        public int? IsDelete { get; set; }

        /// <summary>
        ///多用户编码
        /// </summary>
        [Display(Name = "多用户编码")]
        [Column(TypeName = "int")]
        public int? TenantId { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        [Column(TypeName = "datetime")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        ///直播结束时间
        /// </summary>
        [Display(Name = "直播结束时间")]
        [Column(TypeName = "datetime")]
        public DateTime? LiveEndTime { get; set; }

        /// <summary>
        ///直播开始时间
        /// </summary>
        [Display(Name = "直播开始时间")]
        [Column(TypeName = "datetime")]
        public DateTime? LiveBeginTime { get; set; }

        /// <summary>
        ///长期有效
        /// </summary>
        [Display(Name = "长期有效")]
        [Column(TypeName = "int")]
        public int? IsLongValid { get; set; }

        /// <summary>
        ///课程详情
        /// </summary>
        [Display(Name = "课程详情")]
        [MaxLength(65535)]
        [Column(TypeName = "nvarchar(65535)")]
        public string Detailed { get; set; }

        /// <summary>
        ///直播频道ID
        /// </summary>
        [Display(Name = "直播频道ID")]
        [MaxLength(32)]
        [Column(TypeName = "nvarchar(32)")]
        public string ChannelId { get; set; }

        /// <summary>
        ///课程类型0否1直播
        /// </summary>
        [Display(Name = "课程类型0否1直播")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? IsLive { get; set; }

        /// <summary>
        ///图片缩略图
        /// </summary>
        [Display(Name = "图片缩略图")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string CoursePicMini { get; set; }

        /// <summary>
        ///手机端图片竖图
        /// </summary>
        [Display(Name = "手机端图片竖图")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string CoursePicPhone { get; set; }

        /// <summary>
        ///手机端图片缩略图
        /// </summary>
        [Display(Name = "手机端图片缩略图")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string CoursePicPhoneMini { get; set; }

        /// <summary>
        ///图片
        /// </summary>
        [Display(Name = "图片")]
        [MaxLength(5000)]
        [Column(TypeName = "nvarchar(5000)")]
        public string CoursePicContent { get; set; }

        /// <summary>
        ///录播或者点播的videoId
        /// </summary>
        [Display(Name = "录播或者点播的videoId")]
        [MaxLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string videoId { get; set; }

        /// <summary>
        ///修改时间
        /// </summary>
        [Display(Name = "修改时间")]
        [Column(TypeName = "datetime")]
        public DateTime? ModifyDate { get; set; }


        [Display(Name = "ModifyID")]
        [Column(TypeName = "int")]
        [Editable(true)]
        public int? ModifyID { get; set; }
        /// <summary>
        ///
        /// </summary>
        [Display(Name = "Modifier")]
        [MaxLength(255)]
        [Column(TypeName = "nvarchar(255)")]
        public string Modifier { get; set; }


        [Display(Name = "子课程明细")]
        public List<a_Course_Child> a_Course_Child { get; set; }

    }
}