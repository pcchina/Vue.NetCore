/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下d_WordLibraryService与Id_WordLibraryService中编写
 */
using VOL.AppManager.IRepositories;
using VOL.AppManager.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.AppManager.Services
{
    public partial class d_WordLibraryService : ServiceBase<d_WordLibrary, Id_WordLibraryRepository>, Id_WordLibraryService, IDependency
    {
        public d_WordLibraryService(Id_WordLibraryRepository repository)
             : base(repository) 
        { 
           Init(repository);
        }
        public static Id_WordLibraryService Instance
        {
           get { return AutofacContainerModule.GetService<Id_WordLibraryService>(); }
        }
    }
}
