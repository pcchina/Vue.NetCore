/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下a_Course_ChildService与Ia_Course_ChildService中编写
 */
using VOL.AppManager.IRepositories;
using VOL.AppManager.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.AppManager.Services
{
    public partial class a_Course_ChildService : ServiceBase<a_Course_Child, Ia_Course_ChildRepository>, Ia_Course_ChildService, IDependency
    {
        public a_Course_ChildService(Ia_Course_ChildRepository repository)
             : base(repository) 
        { 
           Init(repository);
        }
        public static Ia_Course_ChildService Instance
        {
           get { return AutofacContainerModule.GetService<Ia_Course_ChildService>(); }
        }
    }
}
