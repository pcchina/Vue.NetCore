/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *Repository提供数据库操作，如果要增加数据库操作请在当前目录下Partial文件夹d_CharLibraryRepository编写代码
 */
using VOL.AppManager.IRepositories;
using VOL.Core.BaseProvider;
using VOL.Core.EFDbContext;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.AppManager.Repositories
{
    public partial class d_CharLibraryRepository : RepositoryBase<d_CharLibrary> , Id_CharLibraryRepository
    {
    public d_CharLibraryRepository(VOLContext dbContext)
    : base(dbContext)
    {

    }
    public static Id_CharLibraryRepository Instance
    {
      get {  return AutofacContainerModule.GetService<Id_CharLibraryRepository>(); } }
    }
}
