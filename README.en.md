# Vue.NetCore

#### Description
框架采用dotnetcore+vue+elementUI 前后端分离，并且支持前端、后台代码业务动态扩展，框架内置了一套有着20多种属性配置的代码生成器，可灵活配置生成的代码，代码生成器界面配置完成即可生成单表/主从表的增、删、改、查、导入、导出、上传、审核基础功能。只需要简单了解即可上手开发

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
